#jerseyDemo


* 运行环境：maven2+、JDK1.6+。
* 使用grizzly容器进行测试。
* 使用了maven中exec-maven-plugin插件。
* git clone https://git.oschina.net/weishengshui/jerseyDemo.git
* mvn exec:java
* http://localhost:8081/myapp/member/xml  即可看到运行效果


maven依赖
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.jersey.demo</groupId>
	<artifactId>jerseyDemo</artifactId>
	<packaging>war</packaging>
	<version>0.0.1-SNAPSHOT</version>
	<name>jerseyDemo Maven Webapp</name>
	<url>http://maven.apache.org</url>

	<dependencies>
		<!-- grizzly -->
		<dependency>
			<groupId>com.sun.jersey</groupId>
			<artifactId>jersey-grizzly</artifactId>
			<version>1.19</version>
		</dependency>
		<dependency>
			<groupId>com.sun.grizzly</groupId>
			<artifactId>grizzly-servlet-webserver</artifactId>
			<version>1.9.45</version>
		</dependency>
		<!-- logger -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.7</version>
		</dependency>
		<!-- jersey -->
		<dependency>
			<groupId>com.sun.jersey</groupId>
			<artifactId>jersey-servlet</artifactId>
			<version>1.19</version>
		</dependency>
		<dependency>
			<groupId>com.sun.jersey</groupId>
			<artifactId>jersey-json</artifactId>
			<version>1.19</version>
		</dependency>
		<dependency>
			<groupId>com.sun.jersey</groupId>
			<artifactId>jersey-client</artifactId>
			<version>1.19</version>
		</dependency>

	</dependencies>
	<build>
		<finalName>jerseyDemo</finalName>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>exec-maven-plugin</artifactId>
				<version>1.2.1</version>
				<executions>
					<execution>
						<goals>
							<goal>java</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<mainClass>com.jersey.demo.Main</mainClass>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>
```

web.xml
```xml
<servlet>
	<servlet-name>jersey</servlet-name>
	<servlet-class>com.sun.jersey.spi.container.servlet.ServletContainer</servlet-class>
	<init-param>
	    <param-name>com.sun.jersey.config.property.packages</param-name>
	    <param-value>com.jersey.demo.resources</param-value>
	</init-param>
	<load-on-startup>1</load-on-startup>
</servlet>

<servlet-mapping>
	<servlet-name>jersey</servlet-name>
	<url-pattern>/jersey/*</url-pattern>
</servlet-mapping>
```

Main.java，负责启动、停止grizzly容器
```java
package com.jersey.demo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.container.grizzly.GrizzlyWebContainerFactory;
import com.sun.jersey.spi.container.servlet.ServletContainer;

public class Main {

	public static final String URI = "http://localhost:8081/myapp";

	public static SelectorThread startServer() throws IllegalArgumentException,
			IOException {
		Map<String, String> initParams = new HashMap<String, String>();
		initParams.put("com.sun.jersey.config.property.packages",
				"com.jersey.demo.resources");

		// 相当于web.xml中servlet jersey的配置
		return GrizzlyWebContainerFactory.create(URI, ServletContainer.class,
				initParams);
	}

	public static void main(String[] args) throws IllegalArgumentException,
			IOException {
		SelectorThread selectorThread = startServer();
		System.out.println("--------jersey server started!");
		System.in.read();

		// stop server
		selectorThread.stopEndpoint();
		System.out.println("--------jersey server stoped!");
	}
}
```


资源类：MemberResource
```java
package com.jersey.demo.resources;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jersey.demo.core.entity.Member;

/**
 * resource class
 * 
 * @author sean
 * 
 */
@Path("/member")
public class MemberResource {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Member createMember() {
		Member member = new Member();
		member.setEmail("657620636@qq.com");
		member.setName("张三");
		member.setStreet("南山区白石洲天河街55号");
		member.setZip("518025");

		return member;
	}

	/**
	 * 输出xml
	 * 
	 * @return
	 */
	@Path("/xml")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member xml() {
		logger.debug("jersey xml");
		return createMember();
	}

	/**
	 * 输出json。不知为何，不加上"; charset=UTF-8"，输出的中文就是乱码
	 * 
	 * @return
	 */
	@Path("/json")
	@GET
	@Produces(value = { MediaType.APPLICATION_JSON + "; charset=UTF-8" })
	public Member json() {
		logger.debug("jersey json");
		return createMember();
	}

	/**
	 * 接收表单请求，输出xml
	 * 
	 * @param name
	 * @return
	 */
	@Path("/formParam")
	@POST
	@Consumes(value = { MediaType.APPLICATION_FORM_URLENCODED })
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member formParam(@FormParam("name") String name) {
		logger.debug("formParam");

		Member member = createMember();
		member.setName(name);
		return member;
	}

	/**
	 * 根据传送过来的json自动封装表单参数到member的属性；输出xml
	 * 
	 * @param member
	 * @return
	 */
	@Path("/formJson")
	@POST
	@Consumes(value = { MediaType.APPLICATION_JSON })
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member formJson(Member member) {
		logger.debug("formJson");
		return member;
	}

	/**
	 * 获取url携带的参数，即"?"之后的参数；输出xml
	 * 
	 * @param name
	 * @return
	 */
	@Path("/queryParam")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member queryParam(@QueryParam("name") String name) {
		logger.debug("queryParam");
		Member member = createMember();
		member.setName(name);
		return member;
	}

	/**
	 * 获取路径参数，输出xml
	 * 
	 * @param name
	 * @return
	 */
	@Path("/pathParam/{name}")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member pathParam(@PathParam("name") String name) {
		logger.debug("pathParam");
		Member member = createMember();
		member.setName(name);
		return member;
	}

	/**
	 * 从上下文获取HttpServletRequest；输出xml
	 * 
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@Path("/request")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML + "; charset=UTF-8" })
	public Member request(@Context HttpServletRequest request)
			throws UnsupportedEncodingException {
		logger.debug("request");
		// 注意从request中获取的参数编码，不经过处理可能就会出现乱码
		String name = new String(request.getParameter("name").getBytes(
				"ISO-8859-1"), "UTF-8");
		Member member = createMember();
		member.setName(name);
		return member;
	}
}
```

测试类
```java
package com.jersey.demo.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.ws.rs.core.MediaType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jersey.demo.Main;
import com.jersey.demo.core.entity.Member;
import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * 使用grizzly容器测试
 * 
 * @author sean
 * 
 */
public class MemberResourceWithGrizzlyContainerTest {

	private final String name = "张三";
	private SelectorThread selectorThread;
	private WebResource wr;

	@Before
	public void setUp() throws Exception {
		selectorThread = Main.startServer();
		Client c = Client.create();
		wr = c.resource(Main.URI + "/member/");
	}

	@After
	public void tearDown() {
		selectorThread.stopEndpoint();
	}

	@Test
	public void testXml() {
		Member member = wr.path("xml").accept(MediaType.APPLICATION_XML)
				.get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	@Test
	public void testJson() {
		Member member = wr.path("json").accept(MediaType.APPLICATION_JSON)
				.get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 测试表单请求参数，改变member的name属性。<br>
	 * requestParam组装form请求参数，似乎没有json方式方便<br>
	 * MediaType.APPLICATION_FORM_URLENCODED，说明是表单请求
	 */
	@Test
	public void testFormParam() {
		String name = "李四";
		String requestParam = "name=" + name;
		Member member = wr.path("formParam")
				.entity(requestParam, MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_XML).post(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 请求参数封装成json。
	 */
	@Test
	public void testFormJson() {
		String name = "李四";
		String zip = "518200";
		Member requestMember = new Member();
		requestMember.setName(name);
		requestMember.setId(3l);
		requestMember.setZip(zip);

		Member member = wr.path("formJson")
				.entity(requestMember, MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_XML).post(Member.class);

		// 验证
		assertEquals(name, member.getName());
		assertTrue(3 == member.getId());
		assertEquals(zip, member.getZip());
	}

	/**
	 * 测试url参数，改变member的name属性。
	 */
	@Test
	public void testQueryParam() {
		String name = "李四";
		Member member = wr.path("queryParam").queryParam("name", name)
				.accept(MediaType.APPLICATION_XML).get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 测试路径参数，改变member的name属性。
	 */
	@Test
	public void testPathParam() {
		String name = "李四";
		Member member = wr.path("pathParam/" + name)
				.accept(MediaType.APPLICATION_XML).get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 测试：从上下文获取HttpServletRequest，并从中获取请求参数
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws ClientHandlerException
	 * @throws UniformInterfaceException
	 */
	@Test
	public void testRequest() throws UniformInterfaceException,
			ClientHandlerException, UnsupportedEncodingException {
		String name = "李四";
		Member member = wr.path("request")
				.queryParam("name", URLEncoder.encode(name, "UTF-8"))
				.accept(MediaType.APPLICATION_XML).get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

}
```
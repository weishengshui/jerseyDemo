package com.jersey.demo.resources;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jersey.demo.core.entity.Member;

/**
 * resource class
 * 
 * @author sean
 * 
 */
@Path("/member")
public class MemberResource {

	private Logger logger = LoggerFactory.getLogger(getClass());

	private Member createMember() {
		Member member = new Member();
		member.setEmail("657620636@qq.com");
		member.setName("张三");
		member.setStreet("南山区白石洲天河街55号");
		member.setZip("518025");

		return member;
	}

	/**
	 * 输出xml
	 * 
	 * @return
	 */
	@Path("/xml")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member xml() {
		logger.debug("jersey xml");
		return createMember();
	}

	/**
	 * 输出json。不知为何，不加上"; charset=UTF-8"，输出的中文就是乱码
	 * 
	 * @return
	 */
	@Path("/json")
	@GET
	@Produces(value = { MediaType.APPLICATION_JSON + "; charset=UTF-8" })
	public Member json() {
		logger.debug("jersey json");
		return createMember();
	}

	/**
	 * 接收表单请求，输出xml
	 * 
	 * @param name
	 * @return
	 */
	@Path("/formParam")
	@POST
	@Consumes(value = { MediaType.APPLICATION_FORM_URLENCODED })
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member formParam(@FormParam("name") String name) {
		logger.debug("formParam");

		Member member = createMember();
		member.setName(name);
		return member;
	}

	/**
	 * 根据传送过来的json自动封装表单参数到member的属性；输出xml
	 * 
	 * @param member
	 * @return
	 */
	@Path("/formJson")
	@POST
	@Consumes(value = { MediaType.APPLICATION_JSON })
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member formJson(Member member) {
		logger.debug("formJson");
		return member;
	}

	/**
	 * 获取url携带的参数，即"?"之后的参数；输出xml
	 * 
	 * @param name
	 * @return
	 */
	@Path("/queryParam")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member queryParam(@QueryParam("name") String name) {
		logger.debug("queryParam");
		Member member = createMember();
		member.setName(name);
		return member;
	}

	/**
	 * 获取路径参数，输出xml
	 * 
	 * @param name
	 * @return
	 */
	@Path("/pathParam/{name}")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML })
	public Member pathParam(@PathParam("name") String name) {
		logger.debug("pathParam");
		Member member = createMember();
		member.setName(name);
		return member;
	}

	/**
	 * 从上下文获取HttpServletRequest；输出xml
	 * 
	 * @param request
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@Path("/request")
	@GET
	@Produces(value = { MediaType.APPLICATION_XML + "; charset=UTF-8" })
	public Member request(@Context HttpServletRequest request)
			throws UnsupportedEncodingException {
		logger.debug("request");
		// 注意从request中获取的参数编码，不经过处理可能就会出现乱码
		String name = new String(request.getParameter("name").getBytes(
				"ISO-8859-1"), "UTF-8");
		Member member = createMember();
		member.setName(name);
		return member;
	}
}

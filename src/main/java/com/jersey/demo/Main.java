package com.jersey.demo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.container.grizzly.GrizzlyWebContainerFactory;
import com.sun.jersey.spi.container.servlet.ServletContainer;

public class Main {

	public static final String URI = "http://localhost:8081/myapp";

	public static SelectorThread startServer() throws IllegalArgumentException,
			IOException {
		Map<String, String> initParams = new HashMap<String, String>();
		initParams.put("com.sun.jersey.config.property.packages",
				"com.jersey.demo.resources");

		// 相当于web.xml中servlet jersey的配置
		return GrizzlyWebContainerFactory.create(URI, ServletContainer.class,
				initParams);
	}

	public static void main(String[] args) throws IllegalArgumentException,
			IOException {
		SelectorThread selectorThread = startServer();
		System.out.println("--------jersey server started!");
		System.in.read();

		// stop server
		selectorThread.stopEndpoint();
		System.out.println("--------jersey server stoped!");
	}
}

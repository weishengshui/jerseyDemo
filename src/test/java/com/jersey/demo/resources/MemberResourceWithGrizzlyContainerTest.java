package com.jersey.demo.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import javax.ws.rs.core.MediaType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.jersey.demo.Main;
import com.jersey.demo.core.entity.Member;
import com.sun.grizzly.http.SelectorThread;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

/**
 * 使用grizzly容器测试
 * 
 * @author sean
 * 
 */
public class MemberResourceWithGrizzlyContainerTest {

	private final String name = "张三";
	private SelectorThread selectorThread;
	private WebResource wr;

	@Before
	public void setUp() throws Exception {
		selectorThread = Main.startServer();
		Client c = Client.create();
		wr = c.resource(Main.URI + "/member/");
	}

	@After
	public void tearDown() {
		selectorThread.stopEndpoint();
	}

	@Test
	public void testXml() {
		Member member = wr.path("xml").accept(MediaType.APPLICATION_XML)
				.get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	@Test
	public void testJson() {
		Member member = wr.path("json").accept(MediaType.APPLICATION_JSON)
				.get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 测试表单请求参数，改变member的name属性。<br>
	 * requestParam组装form请求参数，似乎没有json方式方便<br>
	 * MediaType.APPLICATION_FORM_URLENCODED，说明是表单请求
	 */
	@Test
	public void testFormParam() {
		String name = "李四";
		String requestParam = "name=" + name;
		Member member = wr.path("formParam")
				.entity(requestParam, MediaType.APPLICATION_FORM_URLENCODED)
				.accept(MediaType.APPLICATION_XML).post(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 请求参数封装成json。
	 */
	@Test
	public void testFormJson() {
		String name = "李四";
		String zip = "518200";
		Member requestMember = new Member();
		requestMember.setName(name);
		requestMember.setId(3l);
		requestMember.setZip(zip);

		Member member = wr.path("formJson")
				.entity(requestMember, MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_XML).post(Member.class);

		// 验证
		assertEquals(name, member.getName());
		assertTrue(3 == member.getId());
		assertEquals(zip, member.getZip());
	}

	/**
	 * 测试url参数，改变member的name属性。
	 */
	@Test
	public void testQueryParam() {
		String name = "李四";
		Member member = wr.path("queryParam").queryParam("name", name)
				.accept(MediaType.APPLICATION_XML).get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 测试路径参数，改变member的name属性。
	 */
	@Test
	public void testPathParam() {
		String name = "李四";
		Member member = wr.path("pathParam/" + name)
				.accept(MediaType.APPLICATION_XML).get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

	/**
	 * 测试：从上下文获取HttpServletRequest，并从中获取请求参数
	 * 
	 * @throws UnsupportedEncodingException
	 * @throws ClientHandlerException
	 * @throws UniformInterfaceException
	 */
	@Test
	public void testRequest() throws UniformInterfaceException,
			ClientHandlerException, UnsupportedEncodingException {
		String name = "李四";
		Member member = wr.path("request")
				.queryParam("name", URLEncoder.encode(name, "UTF-8"))
				.accept(MediaType.APPLICATION_XML).get(Member.class);

		// 验证
		assertEquals(name, member.getName());
	}

}
